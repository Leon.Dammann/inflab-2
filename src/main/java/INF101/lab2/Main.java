package INF101.lab2;

import INF101.lab2.pokemon.IPokemon;
import INF101.lab2.pokemon.Pokemon;

public class Main {

    public static IPokemon pokemon1;
    public static IPokemon pokemon2;
    public static void main(String[] args) {
        Main.pokemon1 = new Pokemon("Pikachu");
        Main.pokemon2 = new Pokemon("Oddish");

        while(true){
            if(pokemon1.isAlive() && pokemon2.isAlive()){
                pokemon1.attack(pokemon2);
            }
            else break;
            if(pokemon1.isAlive() && pokemon2.isAlive()){
                pokemon2.attack(pokemon1);
            }
            else break;


        }





    }
}
