package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;    

public class Pokemon implements IPokemon {
    String name;
    int healthPoints;
    int maxHealthPoints;
    int strength;
    Random random;

    public Pokemon(String name){
        this.random = new Random();
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealthPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));

        this.name = name;
    }


    public String getName() {
        return name;
    }

    @Override
    public int getStrength() {
        return strength;
    }

    @Override
    public int getCurrentHP() {
        return healthPoints;
    }

    @Override
    public int getMaxHP() {
        return maxHealthPoints;
    }

    public boolean isAlive() {
        if(healthPoints < 1){
            return false;
        }
        return true;
    }

    @Override
    public void attack(IPokemon target) {
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());
        target.damage(damageInflicted);
        System.out.println(name + " attacks " + target);
        if(!target.isAlive()){
            System.out.println(target + " is defeated by " + name);
        }

    }

    @Override
    public void damage(int damageTaken) {
        if(damageTaken < 0){
            
        }
        else{
        healthPoints = this.healthPoints-damageTaken;
        if(healthPoints < 0){
            healthPoints = 0;
        }
        System.out.println(name + " takes "+ damageTaken + " damage and is left with " + healthPoints + "/"+ maxHealthPoints + " HP");
        }
        
    }

    @Override
    public String toString() {
        return (name + " HP: (" + healthPoints + "/"+ maxHealthPoints + ") STR: " + strength);
    }

}
